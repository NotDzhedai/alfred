package object

// BuiltinFunction holds the type of a built-in function.
type BuiltinFunction func(args ...Object) Object

// Builtin wraps func and implements Object interface.
type Builtin struct {
	// Value holds the function we wrap.
	Fn BuiltinFunction
}

// Type returns the type of this object.
func (b *Builtin) Type() ObjectType { return BUILTIN_OBJ }

// Inspect returns a string-representation of the given object.
func (b *Builtin) Inspect() string { return "builtin function" }
