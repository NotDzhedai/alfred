package object

import "fmt"

// Boolean wraps bool and implements Object and Hashable interface.
type Boolean struct {
	// Value holds the boolean value we wrap.
	Value bool
}

// Inspect returns a string-representation of the given object.
func (b *Boolean) Inspect() string { return fmt.Sprintf("%t", b.Value) }

// Type returns the type of this object.
func (b *Boolean) Type() ObjectType { return BOOLEAN_OBJ }

// HashKey returns a hash key for the given object.
func (b *Boolean) HashKey() HashKey {
	var value uint64
	if b.Value {
		value = 1
	} else {
		value = 0
	}
	return HashKey{Type: b.Type(), Value: value}
}
