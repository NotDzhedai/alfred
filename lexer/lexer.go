// Package lexer contains the code to lex input-programs into a stream
// of tokens, such that they may be parsed.
package lexer

import (
	"log"
	"unicode"

	"gitlab.com/NotDzhedai/alfred/token"
)

// Lexer holds our object-state.
type Lexer struct {
	input        []rune
	position     int  // current position in input
	readPosition int  // current reading position in input
	rn           rune // current rune ander examination
}

// New a Lexer instance from string input.
func New(input string) *Lexer {
	l := &Lexer{input: []rune(input)}
	l.readRune()
	return l
}

// read one forward character
func (l *Lexer) readRune() {
	if l.readPosition >= len(l.input) {
		l.rn = 0
	} else {
		l.rn = l.input[l.readPosition]

	}
	l.position = l.readPosition
	l.readPosition += 1
}

// NextToken to read next token, skipping the white space.
func (l *Lexer) NextToken() token.Token {
	var tok token.Token

	l.skipWhitespace()

	// single-line comment
	if l.rn == '#' || (l.rn == '/' && l.peekRune() == '/') {
		l.skipComment()
		return l.NextToken()
	}

	// multi-line comment
	if l.rn == '/' && l.peekRune() == '*' {

		l.skipMultilineComment()

		log.Println(string(l.rn))

	}

	switch l.rn {
	case '=':
		if l.peekRune() == '=' {
			rn := l.rn
			l.readRune()
			tok = token.Token{Type: token.EQ, Literal: string(rn) + string(l.rn)}
		} else {
			tok = newToken(token.ASSIGN, l.rn)
		}
	case '+':
		if l.peekRune() == '+' {
			rn := l.rn
			l.readRune()
			tok = token.Token{Type: token.PLUS_PLUS, Literal: string(rn) + string(l.rn)}
		} else {
			tok = newToken(token.PLUS, l.rn)
		}
	case '-':
		if l.peekRune() == '-' {
			rn := l.rn
			l.readRune()
			tok = token.Token{Type: token.MINUS_MINUS, Literal: string(rn) + string(l.rn)}
		} else {
			tok = newToken(token.MINUS, l.rn)
		}
	case '!':
		if l.peekRune() == '=' {
			rn := l.rn
			l.readRune()
			tok = token.Token{Type: token.NOT_EQ, Literal: string(rn) + string(l.rn)}
		} else {
			tok = newToken(token.BANG, l.rn)
		}
	case '/':
		tok = newToken(token.SLASH, l.rn)
	case '*':
		tok = newToken(token.ASTERISK, l.rn)
	case '<':
		tok = newToken(token.LT, l.rn)
	case '>':
		tok = newToken(token.GT, l.rn)
	case ';':
		tok = newToken(token.SEMICOLON, l.rn)
	case '(':
		tok = newToken(token.LPAREN, l.rn)
	case ')':
		tok = newToken(token.RPAREN, l.rn)
	case ',':
		tok = newToken(token.COMMA, l.rn)
	case '{':
		tok = newToken(token.LBRACE, l.rn)
	case '}':
		tok = newToken(token.RBRACE, l.rn)
	case '[':
		tok = newToken(token.LBRACKET, l.rn)
	case ']':
		tok = newToken(token.RBRACKET, l.rn)
	case ':':
		tok = newToken(token.COLON, l.rn)
	case '"':
		tok.Type = token.STRING
		tok.Literal = l.readString()
	case 0:
		tok.Literal = ""
		tok.Type = token.EOF
	default:
		if isLetter(l.rn) {
			tok.Literal = l.read(isLetter)
			tok.Type = token.LookupIdent(tok.Literal)
			return tok
		} else if isDigit(l.rn) {
			tok.Type = token.INT
			tok.Literal = l.read(isDigit)
			return tok
		} else {
			tok = newToken(token.ILLEGAL, l.rn)
		}
	}

	l.readRune()
	return tok
}

// skip white space
func (l *Lexer) skipWhitespace() {
	for l.rn == ' ' || l.rn == '\t' || l.rn == '\n' || l.rn == '\r' {
		l.readRune()
	}
}

// skip comment (until the end of the line).
func (l *Lexer) skipComment() {
	for l.rn != '\n' && l.rn != rune(0) {
		l.readRune()
	}
	l.skipWhitespace()
}

func (l *Lexer) skipMultilineComment() {
	found := false

	for !found {
		// break at the end of our input.
		if l.rn == rune(0) {
			found = true
		}

		// otherwise keep going until we find "*/".
		if l.rn == '*' && l.peekRune() == '/' {
			found = true

			// Our current position is "*", so skip
			// forward to consume the "/".
			l.readRune()
		}

		l.readRune()
	}

	l.skipWhitespace()
}

func (l *Lexer) readString() string {
	position := l.position + 1
	for {
		l.readRune()
		if l.rn == '"' || l.rn == 0 {
			break
		}
	}
	return string(l.input[position:l.position])
}

func (l *Lexer) read(f func(rn rune) bool) string {
	position := l.position
	for f(l.rn) {
		l.readRune()
	}
	return string(l.input[position:l.position])
}

func isLetter(rn rune) bool {
	return unicode.IsLetter(rn) || rn == '_'
}

func isDigit(rn rune) bool {
	return '0' <= rn && rn <= '9'
}

// return new token
func newToken(tokenType token.TokenType, rn rune) token.Token {
	return token.Token{Type: tokenType, Literal: string(rn)}
}

func (l *Lexer) peekRune() rune {
	if l.readPosition >= len(l.input) {
		return 0
	} else {
		return l.input[l.readPosition]
	}
}
