package main

import (
	"log"
	"os"
	"path"

	"gitlab.com/NotDzhedai/alfred/evaluator"
	"gitlab.com/NotDzhedai/alfred/lexer"
	"gitlab.com/NotDzhedai/alfred/object"
	"gitlab.com/NotDzhedai/alfred/parser"
	"gitlab.com/NotDzhedai/alfred/repl"
)

func main() {
	args := os.Args[1:]

	if len(args) != 0 {
		filename := args[0]
		ext := path.Ext(filename)
		if ext != ".alb" {
			log.Fatal("Wrong file extantion: must be alb")
		}

		f, err := os.ReadFile(filename)
		if err != nil {
			log.Fatal(err)
		}

		run(string(f))
	} else {
		repl.Start(os.Stdin, os.Stdout)
	}
}

func run(input string) {
	env := object.NewEnvironment()
	l := lexer.New(input)
	p := parser.New(l)
	program := p.ParseProgram()
	if len(p.Errors()) != 0 {
		for _, msg := range p.Errors() {
			log.Println("\t" + msg + "\n")
		}
	}
	evaluated := evaluator.Eval(program, env)
	if evaluated != nil {
		log.Print(evaluated.Inspect() + "\n")
	}
}
