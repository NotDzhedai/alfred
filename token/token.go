// Package token contains constants which are used when lexing a program
// written in the albert language, as done by the parser.
package token

type TokenType string

// Token struct represent the lexer token
type Token struct {
	Type    TokenType
	Literal string
}

// reserved keywords
var keywords = map[string]TokenType{
	"функ":    FUNCTION,
	"хай":     LET,
	"правда":  TRUE,
	"брехня":  FALSE,
	"якщо":    IF,
	"інакше":  ELSE,
	"поверни": RETURN,
	"для":     FOR,
	"длякожн": FOREACH,
	"в":       IN,
}

// LookupIdentifier used to determinate whether identifier is keyword nor not
func LookupIdent(ident string) TokenType {
	if tok, ok := keywords[ident]; ok {
		return tok
	}
	return IDENT
}

// pre-defined Type
const (
	ILLEGAL = "ILLEGAL"
	EOF     = "EOF"

	// identifiers + literals
	IDENT  = "IDENT"
	INT    = "INT"
	STRING = "STRING"

	// Operators
	ASSIGN      = "="
	PLUS        = "+"
	PLUS_PLUS   = "++"
	MINUS       = "-"
	MINUS_MINUS = "--"
	BANG        = "!"
	ASTERISK    = "*"
	SLASH       = "/"
	LT          = "<"
	GT          = ">"
	EQ          = "=="
	NOT_EQ      = "!="

	// Delimiters
	COMMA     = ","
	SEMICOLON = ";"
	LPAREN    = "("
	RPAREN    = ")"
	LBRACE    = "{"
	RBRACE    = "}"
	LBRACKET  = "["
	RBRACKET  = "]"
	COLON     = ":"

	// Keywords
	FUNCTION = "FUNCTION"
	LET      = "LET"
	TRUE     = "TRUE"
	FALSE    = "FALSE"
	IF       = "IF"
	ELSE     = "ELSE"
	RETURN   = "RETURN"
	FOR      = "FOR"
	FOREACH  = "FOREACH"
	IN       = "IN"
)
