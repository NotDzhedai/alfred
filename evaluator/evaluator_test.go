package evaluator

import (
	"testing"

	"gitlab.com/NotDzhedai/alfred/lexer"
	"gitlab.com/NotDzhedai/alfred/object"
	"gitlab.com/NotDzhedai/alfred/parser"
)

func TestEvalIntegerExpression(t *testing.T) {
	tests := []struct {
		input    string
		expected int64
	}{
		{"5", 5},
		{"10", 10},
		{"-5", -5},
		{"-10", -10},
		{"5 + 5 + 5 + 5 - 10", 10},
		{"2 * 2 * 2 * 2 * 2", 32},
		{"-50 + 100 + -50", 0},
		{"5 * 2 + 10", 20},
		{"5 + 2 * 10", 25},
		{"20 + 2 * -10", 0},
		{"50 / 2 * 2 + 10", 60},
		{"2 * (5 + 10)", 30},
		{"3 * 3 * 3 + 10", 37},
		{"3 * (3 * 3) + 10", 37},
		{"(5 + 10 * 2 + 15 / 3) * 2 + -10", 50},
	}

	for _, tt := range tests {
		evaluated := testEval(tt.input)
		testIntegerObject(t, evaluated, tt.expected)
	}
}

func testEval(input string) object.Object {
	l := lexer.New(input)
	p := parser.New(l)
	program := p.ParseProgram()
	env := object.NewEnvironment()

	return Eval(program, env)
}

func testIntegerObject(t *testing.T, obj object.Object, expected int64) bool {
	result, ok := obj.(*object.Integer)
	if !ok {
		t.Errorf("object is not Integer. got=%T (%+v)", obj, obj)
		return false
	}

	if result.Value != expected {
		t.Errorf("object has wrong value. got=%d, want=%d", result.Value, expected)
		return false
	}

	return true
}

func TestEvalBooleanExpression(t *testing.T) {
	tests := []struct {
		input    string
		expected bool
	}{
		{"правда", true},
		{"брехня", false},
		{"1 < 2", true},
		{"1 > 2", false},
		{"1 < 1", false},
		{"1 > 1", false},
		{"1 == 1", true},
		{"1 != 1", false},
		{"1 == 2", false},
		{"1 != 2", true},
		{"правда == правда", true},
		{"брехня == брехня", true},
		{"правда == брехня", false},
		{"правда != брехня", true},
		{"брехня != правда", true},
		{"(1 < 2) == правда", true},
		{"(1 < 2) == брехня", false},
		{"(1 > 2) == правда", false},
		{"(1 > 2) == брехня", true},
	}

	for _, tt := range tests {
		evaluated := testEval(tt.input)
		testBooleanObject(t, evaluated, tt.expected)
	}
}

func testBooleanObject(t *testing.T, obj object.Object, expected bool) bool {
	result, ok := obj.(*object.Boolean)
	if !ok {
		t.Errorf("object is not Boolean. got=%T (%+v)", obj, obj)
		return false
	}

	if result.Value != expected {
		t.Errorf("object has wrong value. got=%t, want=%t", result.Value, expected)
		return false
	}

	return true
}

func TestBangOperator(t *testing.T) {
	tests := []struct {
		input    string
		expected bool
	}{
		{"!правда", false},
		{"!брехня", true},
		{"!5", false},
		{"!!правда", true},
		{"!!брехня", false},
		{"!!5", true},
	}

	for _, tt := range tests {
		evaluated := testEval(tt.input)
		testBooleanObject(t, evaluated, tt.expected)
	}
}

func TestIfElseExpression(t *testing.T) {
	tests := []struct {
		input    string
		expected any
	}{
		{"якщо (правда) { 10 }", 10},
		{"якщо (брехня) { 10 }", nil},
		{"якщо (1) { 10 }", 10},
		{"якщо (1 < 2) { 10 }", 10},
		{"якщо (1 > 2) { 10 }", nil},
		{"якщо (1 > 2) { 10 } інакше { 20 }", 20},
		{"якщо (1 < 2) { 10 } інакше { 20 }", 10},
	}

	for _, tt := range tests {
		evaluated := testEval(tt.input)
		integer, ok := tt.expected.(int)
		if ok {
			testIntegerObject(t, evaluated, int64(integer))
		} else {
			testNullObject(t, evaluated)
		}
	}
}

func testNullObject(t *testing.T, obj object.Object) bool {
	if obj != NULL {
		t.Errorf("object is not NULL. got=%T (%+v)", obj, obj)
		return false
	}
	return true
}

func TestReturnStatement(t *testing.T) {
	tests := []struct {
		input    string
		expected int64
	}{
		{"поверни 10;", 10},
		{"поверни 10; 9", 10},
		{"поверни 2 * 5; 9", 10},
		{"9; поверни 2 * 5; 9", 10},
		{
			`
			якщо (10 > 1) {
				якщо (10 > 1) {
					поверни 10;
				}
				поверни 1;
			}
			`,
			10,
		},
		{
			`
			хай фуууу = функ(x) {
				поверни x;
  				x + 10;
			};
			фуууу(10);`,
			10,
		},
		{
			`
			хай f = функ(x) {
   				хай результат = x + 10;
   				поверни результат;
   				поверни 10;
			};
			f(10);`,
			20,
		},
	}

	for _, tt := range tests {
		evaluated := testEval(tt.input)
		testIntegerObject(t, evaluated, tt.expected)
	}
}

func TestErrorHandling(t *testing.T) {
	tests := []struct {
		input           string
		expectedMessage string
	}{
		{
			"5 + правда;",
			"type mismatch: INTEGER + BOOLEAN",
		},
		{
			"5 + правда; 5;",
			"type mismatch: INTEGER + BOOLEAN",
		},
		{
			"-правда",
			"unknown operator: -BOOLEAN",
		},
		{
			"правда + брехня;",
			"unknown operator: BOOLEAN + BOOLEAN",
		},
		{
			"5; правда + брехня; 5",
			"unknown operator: BOOLEAN + BOOLEAN",
		},
		{
			"якщо (10 > 1) { правда + брехня; }",
			"unknown operator: BOOLEAN + BOOLEAN",
		},
		{
			`
			якщо (10 > 1) {
				якщо (10 > 1) {
					поверни правда + брехня;
				}
				поверни 1;
			}
			`,
			"unknown operator: BOOLEAN + BOOLEAN",
		},
		{
			"foobar",
			"identifier not found: foobar",
		},
		{
			`"Hello" - "world"`,
			"unknown operator: STRING - STRING",
		},
		{
			`{"name": "Monkey"}[функ(x) { x }];`,
			"unusable as hash key: FUNCTION",
		},
	}

	for _, tt := range tests {
		evaluated := testEval(tt.input)

		errObj, ok := evaluated.(*object.Error)
		if !ok {
			t.Errorf("no error object returned. got=%T(%+v)", evaluated, evaluated)
			continue
		}

		if errObj.Message != tt.expectedMessage {
			t.Errorf("wrong error message. expected=%q, got=%q", tt.expectedMessage, errObj.Message)
		}
	}
}

func TestLetStatements(t *testing.T) {
	tests := []struct {
		input    string
		expected int64
	}{

		{"хай a = 5; a;", 5},
		{"хай a = 5 * 5; a;", 25},
		{"хай a = 5; хай b = a; b;", 5},
		{"хай a = 5; хай b = a; хай c = a + b + 5; c;", 15},
		{"хай a = 1; a++; a", 2},
		{"хай a = 1; a--; a", 0},
	}

	for _, tt := range tests {
		testIntegerObject(t, testEval(tt.input), tt.expected)
	}
}

func TestFunctionObject(t *testing.T) {
	input := "функ(x) {x + 2};"

	evaluated := testEval(input)
	fn, ok := evaluated.(*object.Function)
	if !ok {
		t.Fatalf("object is not Function. got=%T (%+c)", evaluated, evaluated)
	}

	if len(fn.Parameters) != 1 {
		t.Fatalf("function has wrong parameters. Parameters=%+v", fn.Parameters[0])
	}

	if fn.Parameters[0].String() != "x" {
		t.Fatalf("parameter is not 'x'. got=%q", fn.Parameters[0])
	}

	expectedBody := "(x + 2)"

	if fn.Body.String() != expectedBody {
		t.Fatalf("body is not %q. got=%q", expectedBody, fn.Body.String())
	}
}

func TestFunctionApplication(t *testing.T) {
	tests := []struct {
		input    string
		expected any
	}{
		{"хай identity = функ(x) { x; }; identity(5);", 5},
		{"хай identity = функ(x) { поверни x; }; identity(5);", 5},
		{"хай double = функ(x) { x * 2; }; double(5);", 10},
		{"хай add = функ(x, y) { x + y; }; add(5, 5);", 10},
		{"хай add = функ(x, y) { x + y; }; add(5 + 5, add(5, 5));", 20},
		{"функ(x) { x; }(5)", 5},
		{`функ(x) { x; }("hi")`, "hi"},
	}
	for _, tt := range tests {
		switch exp := tt.expected.(type) {
		case int:
			testIntegerObject(t, testEval(tt.input), int64(exp))
		case string:
			testStringObject(t, testEval(tt.input), exp)
		default:
			t.Fatalf("unknown type: %T", exp)
		}
	}
}

func TestClosures(t *testing.T) {
	input := `
	хай newAdder = функ(x) {
		функ(y) { x + y };
	};
	хай addTwo = newAdder(2);
	addTwo(2);`
	testIntegerObject(t, testEval(input), 4)
}

func TestStringLiteral(t *testing.T) {
	input := `"Hello world";`

	testStringObject(t, testEval(input), "Hello world")
}

func TestStringConcatenation(t *testing.T) {
	input := `"Hello" + " " + "world"`

	testStringObject(t, testEval(input), "Hello world")
}

func testStringObject(t *testing.T, obj object.Object, expected string) {
	str, ok := obj.(*object.String)
	if !ok {
		t.Fatalf("object is not String. got=%T (%+v)", str, str)
	}

	if str.Value != expected {
		t.Errorf("String has wrong value. got=%q", str.Value)
	}
}

func TestBuiltinFunctions(t *testing.T) {
	tests := []struct {
		input    string
		expected any
	}{

		{`довжина("")`, 0},
		{`довжина("four")`, 4},
		{`довжина("hello world")`, 11},
		{`довжина(1)`, "argument to `len` not supported, got INTEGER"},
		{`довжина("one", "two")`, "wrong number of arguments. got=2, want=1"},
		{`довжина([1, 2, 3])`, 3},
		{`довжина([])`, 0},
		{`виведи("hello", "world!")`, nil},
		{`перш([1, 2, 3])`, 1},
		{`перш([])`, nil},
		{`перш(1)`, "argument to `first` must be ARRAY, got INTEGER"},
		{`остн([1, 2, 3])`, 3},
		{`остн([])`, nil},
		{`остн(1)`, "argument to `last` must be ARRAY, got INTEGER"},
		{`решта([1, 2, 3])`, []int{2, 3}},
		{`решта([])`, nil},
		{`додай(1, 1)`, "argument to `push` must be ARRAY, got INTEGER"},
		{`додай([], 1)`, []int{1}},
	}

	for _, tt := range tests {
		evaluated := testEval(tt.input)

		switch exp := tt.expected.(type) {
		case int:
			testIntegerObject(t, evaluated, int64(exp))
		case nil:
			testNullObject(t, evaluated)
		case string:
			errObj, ok := evaluated.(*object.Error)
			if !ok {
				t.Errorf("object is not Error. got=%T (%+v)", evaluated, evaluated)
				continue
			}

			if errObj.Message != exp {
				t.Errorf("wrong error message. expected=%q, got=%q", exp, errObj.Message)
			}
		case []int:
			array, ok := evaluated.(*object.Array)
			if !ok {
				t.Errorf("obj not Array. got=%T (%+v)", evaluated, evaluated)
				continue
			}

			if len(array.Elements) != len(exp) {
				t.Errorf("wrong num of elements. want=%d, got=%d", len(exp), len(array.Elements))
				continue
			}

			for i, expectedElem := range exp {
				testIntegerObject(t, array.Elements[i], int64(expectedElem))
			}
		}
	}
}

func TestArrayLiterals(t *testing.T) {
	input := "[1, 2 * 2, 3 + 3]"

	evaluated := testEval(input)

	result, ok := evaluated.(*object.Array)
	if !ok {
		t.Fatalf("object is not Array. got=%T (%+v)", evaluated, evaluated)
	}

	if len(result.Elements) != 3 {
		t.Fatalf("array has wrong num of elements. got=%d", len(result.Elements))
	}

	testIntegerObject(t, result.Elements[0], 1)
	testIntegerObject(t, result.Elements[1], 4)
	testIntegerObject(t, result.Elements[2], 6)
}

func TestArrayIndexExpressions(t *testing.T) {
	tests := []struct {
		input    string
		expected any
	}{

		{
			"[1, 2, 3][0]",
			1,
		},
		{
			"[1, 2, 3][1]",
			2,
		},
		{
			"[1, 2, 3][2]",
			3,
		},
		{
			"хай i = 0; [1][i];",
			1,
		},
		{
			"[1, 2, 3][1 + 1];",
			3,
		},
		{
			"хай myArray = [1, 2, 3]; myArray[2];",
			3,
		},
		{
			"хай myArray = [1, 2, 3]; myArray[0] + myArray[1] + myArray[2];",
			6,
		},
		{
			"хай myArray = [1, 2, 3]; хай i = myArray[0]; myArray[i]",
			2,
		},
		{
			"[1, 2, 3][3]",
			nil,
		},
		{
			"[1, 2, 3][-1]",
			nil,
		},
	}

	for _, tt := range tests {
		evaluated := testEval(tt.input)
		integer, ok := tt.expected.(int)
		if ok {
			testIntegerObject(t, evaluated, int64(integer))
		} else {
			testNullObject(t, evaluated)
		}
	}
}

func TestHashLiterals(t *testing.T) {
	input := `хай two = "two";
	{
		"one": 10 - 9,
		two: 1 + 1,
		"thr" + "ee": 6 / 2,
		4: 4,
		правда: 5,
		брехня: 6
	}
	`
	evaluated := testEval(input)
	result, ok := evaluated.(*object.Hash)
	if !ok {
		t.Fatalf("Eval didn't return Hash. got=%T (%+v)", evaluated, evaluated)
	}
	expected := map[object.HashKey]int64{
		(&object.String{Value: "one"}).HashKey():   1,
		(&object.String{Value: "two"}).HashKey():   2,
		(&object.String{Value: "three"}).HashKey(): 3,
		(&object.Integer{Value: 4}).HashKey():      4,
		TRUE.HashKey():                             5,
		FALSE.HashKey():                            6,
	}
	if len(result.Pairs) != len(expected) {
		t.Fatalf("Hash has wrong num of pairs. got=%d", len(result.Pairs))
	}
	for expectedKey, expectedValue := range expected {
		pair, ok := result.Pairs[expectedKey]
		if !ok {
			t.Errorf("no pair for given key in Pairs")
		}
		testIntegerObject(t, pair.Value, expectedValue)
	}
}

func TestHashIndexExpressions(t *testing.T) {
	tests := []struct {
		input    string
		expected interface{}
	}{
		{
			`{"foo": 5}["foo"]`,
			5,
		},
		{
			`{"foo": 5}["bar"]`,
			nil,
		},
		{
			`хай key = "foo"; {"foo": 5}[key]`,
			5,
		},
		{
			`{}["foo"]`,
			nil,
		},
		{
			`{5: 5}[5]`,
			5,
		},
		{
			`{правда: 5}[правда]`,
			5,
		},
		{
			`{брехня: 5}[брехня]`,
			5,
		},
	}

	for _, tt := range tests {
		evaluated := testEval(tt.input)
		integer, ok := tt.expected.(int)
		if ok {
			testIntegerObject(t, evaluated, int64(integer))
		} else {
			testNullObject(t, evaluated)
		}
	}
}

func TestForLoop(t *testing.T) {
	input := `
хай x = 1;
хай sum = 0;
хай up = 100;
для (x < up) {
	хай sum = sum + x;
	хай x = x + 1;
}
sum
`
	evaluated := testEval(input)
	testIntegerObject(t, evaluated, 4950)
}

func TestForeach(t *testing.T) {
	input := `
		хай масив = [1, 1, 1, 1];
		хай сума = 0;
		длякожн ітем в масив {
			хай сума = сума + ітем
		}
		сума
	`
	evaluated := testEval(input)
	testIntegerObject(t, evaluated, 4)
}

func TestForeachWithIndex(t *testing.T) {
	input := `
		хай масив = [1, 1, 1, 1];
		хай сума = 0;
		длякожн індекс, ітем в масив {
			хай сума = сума + індекс
		}
		сума
	`
	evaluated := testEval(input)
	testIntegerObject(t, evaluated, 6)
}
